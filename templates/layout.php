<html>
<head>
    <title>Shopping</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="public/style.css" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="jumbotron text-center">
        <h1>Lista sklepów</h1>
        <p>Lista zakupów w danym sklepie</p>
    </div>
    <div class="container container-height">
        <div class="row">
            <div class="side-nav col-sm-2">
                <ulclass="list-group">
                <li class="list-group-item">
                    <a href="/shopping/">Lista sklepów</a>
                </li>
                <li class="list-group-item">
                    <a href="/shopping/?action=create">Dodaj sklep</a>
                </li>
                </ul>
            </div>
            <div class="col-sm-10">
                <?php
                require_once ("templates/pages/$page.php");
                ?>
            </div>
        </div>
    </div>
    <footer class="container-fluid">
        <p>Zakupy - projekt zrobiony na podstawie kursu PHP</p>
    </footer>
</body>
</html>