<div>
    <?php $shop = $params['shop'] ?? null; ?>
    <?php if($shop): ?>
        <div >Sklep: <?php echo $shop['shop']?></div>
        <div >Lista zakupów: <?php echo $shop['shopping_list']?></div>
        <a href="/shopping/?action=edit&id=<?php echo $shop['id'] ?>">
            <button class="btn btn-primary">Edytuj</button>
        </a>
    <?php else: ?>
        <div>Brak notatki do wyświetlenia</div>
    <?php endif; ?>
    <a href="/shopping/">
        <button class="btn btn-primary">Powrót do listy sklepów</button>
    </a>
</div>