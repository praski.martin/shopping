<section>
    <h3>lista sklepów</h3>
    <?php
    if (!empty($params['error'])){
        switch ($params['error']){
            case 'missingShopId':
                echo 'Niepoprawny identyfikator sklepu';
                break;
            case 'shopNotFound':
                echo 'Notatka nie została znaleziona';
                break;
        }
    }
    if (!empty($params['before'])){
        switch ($params['before']){
            case 'created':
                echo 'Notatka została utworzona';
                break;
            case 'edited':
                echo 'Notatka została zaktualizowana';
                break;
            case 'deleted':
                echo 'Notatka została usunięta';
                break;
        }
    }
    ?>
    <?php
    $sort = $params['sort'];
    $by = $sort['by'] ?? 'shop';
    $order = $sort['order'] ?? 'desc';

    $page = $params['page'] ?? [];
    $size = $page['size'] ?? 10;
    $currentPage = $page['number'] ?? 1;
    $pages = $page['pages'] ?? 1;
    $phrase = $params['phrase'] ?? null;
    ?>
    <form action="/shopping/" method="GET">
        <div class="form-check">
            <label>Wyszukaj: <input type="text" name="phrase" value="<?php echo $phrase ?>"></label>
        </div>
        <div class="form-check">
            <div>Sortuj po:</div>
            <label class="form-check-label">Sklepie:<input name="sortby" type="radio" value="shop" <?php echo $by ==='shop' ? 'checked' : '' ?>/></label>
            <label class="form-check-label">Dacie:<input name="sortby" type="radio" value="created" <?php echo $by === 'created' ? 'checked' : '' ?>/></label>
        </div>
        <div class="form-check">
            <div>Kierunek sortowania</div>
            <label class="form-check-label">Rosnąco:<input name="sortorder" type="radio" value="asc" <?php echo $order ==='asc' ? 'checked' : '' ?>/></label>
            <label class="form-check-label">Malejąco:<input name="sortorder" type="radio" value="desc" <?php echo $order ==='desc' ? 'checked' : '' ?>/></label>
        </div>
        <div class="form-check">
            <div>Rozmiar paczki</div>
            <label>1<input name="pagesize" type="radio" value="1" <?php echo $size === 1 ? 'checked' : '' ?>/></label>
            <label>5<input name="pagesize" type="radio" value="5" <?php echo $size === 5 ? 'checked' : '' ?>/></label>
            <label>10<input name="pagesize" type="radio" value="10" <?php echo $size === 10 ? 'checked' : '' ?>/></label>
            <label>25<input name="pagesize" type="radio" value="25" <?php echo $size === 25 ? 'checked' : '' ?>/></label>
        </div>
        <input class="btn btn-primary" type="submit" value="Wyślij">
    </form>

    <?php echo $params['resultList'] ?? "" ?>
    <table class="table">
        <thead>
            <th>Id</th>
            <th>Sklep</th>
            <th>Data</th>
            <th>Opcje</th>
        </thead>
        <tbody>
            <?php foreach($params['shops'] ?? [] as $shop): ?>
                <tr>
                    <td><?php echo $shop['id'] ?></td>
                    <td><?php echo $shop['shop'] ?></td>
                    <td><?php echo $shop['created'] ?></td>
                    <td>
                        <a href="/shopping/?action=show&id=<?php echo (int) $shop['id'] ?>">
                            <button class="btn btn-info">Szczegóły</button>
                        </a>
                        <a href="/shopping/?action=delete&id=<?php echo (int) $shop['id'] ?>">
                            <button class="btn btn-info">Usuń</button>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php
        $paginationUrl = "&phrase=$phrase&pagesize=$size?sortby=$by&sortorder=$order";
    ?>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <?php if ($currentPage !== 1) : ?>
            <li class="page-item">
                <a class="page-link" href="/shopping/?page=<?php echo $currentPage - 1 . $paginationUrl ?>">
                    <button>Poprzednia</button>
                </a>
            </li>
            <?php endif; ?>
            <?php for ($i = 1; $i <= $pages; $i++): ?>
                <li class="page-item">
                    <a class="page-link active" href="/shopping/?page=<?php echo $i . $paginationUrl ?>">
                        <button><?php echo $i; ?></button>
                    </a>
                </li>
            <?php endfor; ?>
            <?php if ($currentPage < $pages) : ?>
            <li class="page-item">
                <a class="page-link" href="/shopping/?page=<?php echo $currentPage + 1 . $paginationUrl ?>">
                    <button>Następna</button>
                </a>
            </li>
            <?php endif; ?>
        </ul>
    </nav>
</section>