<h3>Edycja notatki</h3>
<form action="/shopping/?action=edit" method="post">
    <?php if (!empty($params['shop'])): ?>
    <?php $shop = $params['shop']?>
    <input name="id" type="hidden" value="<?php echo $shop['id'] ?>"/>
    <div class="form-group">
        <label>Nazwa sklepu<span class="required">*</span></label>
        <input class="form-control" type="text" name="shop" value="<?php echo $shop['shop'] ?>">
    </div>
    <div class="form-group">
        <label>Lista zakupów</label>
        <textarea class="form-control" name="shoppingList"><?php echo $shop['shopping_list'] ?></textarea>
    </div>
    <div>
        <input class="btn btn-primary" type="submit" value="submit">
    </div>
    <?php else: ?>
    <div>
        Brak danych do wyświetlenia.
        <a href="/shopping/">
            <button>Powrót do listy notatek</button>
        </a>
    </div>
    <?php endif; ?>
</form>