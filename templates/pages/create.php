    <h3>Dodaj nowy sklep</h3>
    <form action="/shopping/?action=create" method="post">
        <div class="form-group">
            <label>Nazwa sklepu<span class="required">*</span></label>
            <input class="form-control" type="text" name="shop">
        </div>
        <div class="form-group">
            <label>Lista zakupów</label>
            <textarea class="form-control" name="shoppingList"> </textarea>
        </div>
        <div>
            <input class="btn btn-primary" type="submit" value="submit">
        </div>
    </form>