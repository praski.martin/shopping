<?php

declare(strict_types=1);

spl_autoload_register(function (string $classNameSpace){
    $path = str_replace(['\\','App/'],['/',''],$classNameSpace);
       $path="src/$path.php";
       require_once($path);
});

require_once("src/debug.php");

$configuration = require_once ("config/config.php");

use App\Controller\AbstractController;
use App\Controller\ShopController;
use App\Exception\AppException;
use App\Exception\ConfigurationException;
use App\Exception\StorageException;
use App\Request;



$request = new Request($_GET, $_POST, $_SERVER);

try {
    AbstractController::initConfiguration($configuration);
    $controller = new ShopController($request);
    $controller->run();
}catch (ConfigurationException $e){
    echo '<h1>Wystąpił błąd w aplikacji</h1>';
    echo 'Problem z konfiguracją. Proszę skontaktować się z administratorem';
} catch (AppException $e){
    echo "Wystąpił błąd w aplikacji";
    echo '<h3>'.$e->getMessage().'</h3>';
}catch (\Throwable $e){
    echo "Wystąpił błąd w aplikacji";
}