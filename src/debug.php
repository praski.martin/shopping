<?php

declare(strict_types = 1);

error_reporting(E_ALL);
ini_set('display_errors','1');

function dump($data)
{
    echo '<div
        style.css="
        background-color: #999999;
        padding:0px 10px;
        display: inline-block;
        border: 1px solid gray;
        "
    >
    <pre>';
    print_r($data);
    echo '</pre></div></br>';
}