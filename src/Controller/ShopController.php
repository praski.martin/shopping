<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\NotFoundException;

class ShopController extends AbstractController
{
    private const PAGE_SIZE = 10;

    public function createAction()
    {

        if ($this->request->hasPost()){
            $shopData =[
                'shop' => $this->request->postParam('shop'),
                'shoppingList' => $this->request->postParam('shoppingList')
            ];
            $this->noteModel->create($shopData);
            $this->redirect('/',['before' => 'created']);
        }
        $this->view->render('create');
    }

    public function showAction()
    {
        $this->view->render('show', ['shop' => $this->getShop()]);
    }

    public function listAction(): void
    {
        $phrase = $this->request->getParam('phrase');
        $pageNumber = (int)$this->request->getParam('page', 1);
        $pageSize = (int) $this->request->getParam('pagesize', self::PAGE_SIZE); 
        $sortBy = $this->request->getParam('sortby', 'shop');
        $sortOrder = $this->request->getParam('sortorder', 'desc');

        if (!in_array($pageSize, [1, 5, 10, 25])){
            $pageSize = self::PAGE_SIZE;
        }

        if ($phrase){
            $shopList = $this->noteModel->search($phrase, $pageNumber, $pageSize, $sortBy, $sortOrder);
            $shops = $this->noteModel->searchCount($phrase);
        }else{
            $shopList = $this->noteModel->list($pageNumber, $pageSize, $sortBy, $sortOrder);
            $shops = $this->noteModel->count();
        }

        $this->view->render(
            'list',
            [
                'page' => [
                    'number' => $pageNumber,
                    'size' => $pageSize,
                    'pages' => (int) ceil($shops/$pageSize)
                ],
                'phrase' => $phrase,
                'sort' => [
                    'by' => $sortBy,
                    'order' => $sortOrder
                ],
                'shops' => $shopList,
                'before' => $this->request->getParam('before'),
                'error' => $this->request->getParam('error')
            ]
        );
    }

    public function editAction()
    {
        if ($this->request->isPost()){
            $shopId =(int) $this->request->postParam('id');
            $shopData =[
                'shop' => $this->request->postParam('shop'),
                'shoppingList' => $this->request->postParam('shoppingList')
            ];
            $this->noteModel->edit($shopId, $shopData);
            $this->redirect('/',['before' => 'edited'] );
        }

        $this->view->render(
            'edit',
            ['shop' => $this->getShop()]
        );
    }

    public function deleteAction(): void
    {
        if ($this->request->isPost()){
            $id = (int) $this->request->postParam('id');
            $this->noteModel->delete($id);
            $this->redirect('/',['before' => 'deleted']);
        }
        $this->view->render(
            'delete',
            ['shop' => $this->getShop()]
        );
    }

    final private function getShop(): array
    {
        $shopId = (int) $this->request->getParam('id');
        if (!$shopId){
            $this->redirect('/',['error' => 'missingShopId']);
        }

        return $this->noteModel->get($shopId);
    }

}