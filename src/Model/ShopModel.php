<?php

declare(strict_types=1);

namespace App\Model;

use App\Exception\StorageException;
use App\Exception\NotFoundException;
use PDO;
use Throwable;

class ShopModel extends AbstractModel implements ModelInterface
{
    public function list(int $pageNumber, int $pageSize, string $sortBy, string $sortOrder): array
    {
        return $this->findBy(null , $pageNumber, $pageSize, $sortBy, $sortOrder );
    }

    public function search(
        string $phrase,
        int $pageNumber,
        int $pageSize,
        string $sortBy,
        string $sortOrder
    ): array{
        return $this->findBy($phrase, $pageNumber, $pageSize, $sortBy, $sortOrder );
    }

    public function count(): int
    {
        try {

            $query = "
                SELECT count(*) AS cn
                FROM shopping 
            ";
            $result = $this->conn->query($query);
            $result = $result->fetch(PDO::FETCH_ASSOC);

            return (int) $result['cn'];
        }catch (Throwable $e){
            new StorageException('Nie udało się pobrać informacji o liczbie sklepów', 400, $e);
        }
    }

    public function searchCount(string $phrase): int
    {
        try {
            $phrase=$this->conn->quote('%'.$phrase .'%', PDO::PARAM_STR);
            $query = "
                SELECT count(*) AS cn
                FROM shopping
                WHERE shop LIKE($phrase)
            ";
            $result = $this->conn->query($query);
            $result = $result->fetch(PDO::FETCH_ASSOC);

            return (int) $result['cn'];
        }catch (Throwable $e){
            new StorageException('Nie udało się pobrać informacji o liczbie sklepów', 400, $e);
        }
    }

    public function get(int $id): array
    {
        try {
            $query = "SELECT * FROM shopping WHERE id = $id";
            $result = $this->conn->query($query);
            $shop= $result->fetch(PDO::FETCH_ASSOC);
        }catch (Throwable $e){
            throw new StorageException('Nie udało się pobrać notatki', 400, $e);
        }
        if (!$shop){
            throw new \NotFoundException("Sklep o id: $id");
            exit('nie ma takiego sklepu');
        }
        return $shop;
    }

    public function create(array $data): void
    {
        try {
            $shop=$this->conn->quote($data['shop']);
            $shoppingList=$this->conn->quote($data['shoppingList']);
            $created= $this->conn->quote(date('Y-m-d H:i:s'));
            $query = "INSERT INTO shopping(shop, shopping_list, created) VALUES($shop, $shoppingList, $created)";

            $this->conn->exec($query);

        }catch (Throwable $e){
            throw new StorageException('Nie udało się utworzyć nowej notatki', 400,$e);
        }
    }

    public function edit(int $id, array $data): void
    {
        try {
            $shop = $this->conn->quote($data['shop']);
            $shoppingList=$this->conn->quote($data['shoppingList']);
            $query = "
                UPDATE shopping
                SET shop = $shop, shopping_list = $shoppingList
                WHERE id = $id
            ";
            $this->conn->exec($query);
        }catch (Throwable $e){
            throw new StorageException('Nie udało się zaktualizować notatki', 400, $e);
        }
    }

    public function delete(int $id): void
    {
        try {
            $query = "DELETE FROM shopping WHERE id = $id LIMIT 1";
            $this->conn->exec($query);
        }catch (Throwable $e){
            throw new StorageException('Nie udało się usunąć sklepu', 400, $e);
        }
    }

    private function findBy(
        ?string $phrase,
        int $pageNumber,
        int $pageSize,
        string $sortBy,
        string $sortOrder
    ): array{
        try {
            $limit = $pageSize;
            $offset = ($pageNumber -1) * $pageSize;
            if (!in_array($sortBy,['created', 'shop'])){
                $sortBy = 'shop';
            }

            if (!in_array($sortOrder,['asc', 'desc'])){
                $sortOrder = 'desc';
            }

            $wherePart = '';
            if ($phrase){
                $phrase=$this->conn->quote('%'.$phrase.'%', PDO::PARAM_STR);
                $wherePart = "WHERE shop LIKE ($phrase)";
            }


            $query = "
                SELECT id, shop, created 
                FROM shopping 
                $wherePart
                ORDER BY $sortBy $sortOrder
                LIMIT $offset, $limit
            ";

            $result = $this->conn->query($query);
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }catch (Throwable $e){
            new StorageException('Nie udało się pobrać sklepów', 400, $e);
        }
    }
}